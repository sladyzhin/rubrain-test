package com.rubrain.test.service;

import com.rubrain.test.controller.dto.AccountDto;

public interface AccountService {

    void openAccount(String accountNumber);

    AccountDto getBalance(String accountNumber);

}
