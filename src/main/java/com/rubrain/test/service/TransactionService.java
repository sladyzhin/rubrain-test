package com.rubrain.test.service;

import com.rubrain.test.controller.dto.TransactionDto;
import com.rubrain.test.controller.dto.TransactionType;

public interface TransactionService {

    void createAndExecuteTransaction(TransactionType type, TransactionDto transactionDto);

}
