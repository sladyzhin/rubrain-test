package com.rubrain.test.service.converter;

import com.rubrain.test.controller.dto.AccountDto;
import com.rubrain.test.repository.entity.AccountEntity;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AccountConverter implements Converter<AccountEntity, AccountDto> {

    @Override
    public AccountDto convert(AccountEntity source) {
        return AccountDto.builder()
                .number(source.getNumber())
                .amount(source.getAmount())
                .build();
    }
}
