package com.rubrain.test.service.impl;

import com.rubrain.test.controller.dto.TransactionDto;
import com.rubrain.test.controller.dto.TransactionType;
import com.rubrain.test.exception.InternalException;
import com.rubrain.test.repository.AccountRepository;
import com.rubrain.test.repository.entity.AccountEntity;
import com.rubrain.test.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final AccountRepository accountRepository;

    @Override
    public void createAndExecuteTransaction(TransactionType type, TransactionDto transactionDto) {

        AccountEntity accountEntity = accountRepository.findByNumber(transactionDto.getAccountNumber())
                .orElseThrow(() -> new InternalException("Account update failed because it was not found"));

        updateAmount(type, accountEntity, transactionDto.getAmount());

        accountRepository.save(accountEntity);
    }

    private void updateAmount(TransactionType type, AccountEntity accountEntity, BigDecimal transactionAmount) {

        BigDecimal amount = accountEntity.getAmount();

        if (type == TransactionType.WITHDRAWAL) {
            validateWithdrawalTransaction(amount, transactionAmount);
            accountEntity.setAmount(amount.subtract(transactionAmount));
        } else {
            accountEntity.setAmount(amount.add(transactionAmount));
        }
    }

    private void validateWithdrawalTransaction(BigDecimal accountAmount, BigDecimal transactionAmount) {
        if (accountAmount.subtract(transactionAmount).compareTo(BigDecimal.ZERO) < 0) {
            throw new InternalException("Not enough money on account");
        }
    }
}
