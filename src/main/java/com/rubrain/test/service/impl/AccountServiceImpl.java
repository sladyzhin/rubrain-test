package com.rubrain.test.service.impl;

import com.rubrain.test.controller.dto.AccountDto;
import com.rubrain.test.exception.InternalException;
import com.rubrain.test.repository.AccountRepository;
import com.rubrain.test.repository.entity.AccountEntity;
import com.rubrain.test.service.AccountService;
import com.rubrain.test.service.converter.AccountConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final AccountConverter accountConverter;

    @Override
    public void openAccount(String accountNumber) {
        accountRepository.save(AccountEntity.builder()
                .number(accountNumber)
                .amount(BigDecimal.ZERO)
                .build());
    }

    @Override
    public AccountDto getBalance(String accountNumber) {
        return accountRepository.findByNumber(accountNumber)
                .map(accountConverter::convert)
                .orElseThrow(() -> new InternalException("Account not found", HttpStatus.NOT_FOUND.value()));
    }
}
