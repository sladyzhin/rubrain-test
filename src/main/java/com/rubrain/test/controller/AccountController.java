package com.rubrain.test.controller;

import com.rubrain.test.controller.dto.AccountDto;
import com.rubrain.test.controller.dto.OpenAccountRequest;
import com.rubrain.test.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RestController
@RequiredArgsConstructor
public class AccountController {

    private static final String ACCOUNTS_URI = "/accounts";
    private static final String ACCOUNT_BY_NUMBER_URI = "/accounts/{accountNumber}";

    private final AccountService accountService;

    @GetMapping(ACCOUNT_BY_NUMBER_URI)
    public AccountDto getBalance(@PathVariable String accountNumber) {
        return accountService.getBalance(accountNumber);
    }

    @PostMapping(ACCOUNTS_URI)
    public void open(@RequestBody @Valid OpenAccountRequest openAccountRequest) {
        accountService.openAccount(openAccountRequest.getNumber());
    }

}
