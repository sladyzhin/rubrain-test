package com.rubrain.test.controller.dto;

public enum TransactionType {
    WITHDRAWAL,
    DEPOSIT
}
