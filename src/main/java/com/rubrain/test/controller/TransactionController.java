package com.rubrain.test.controller;

import com.rubrain.test.controller.dto.TransactionDto;
import com.rubrain.test.controller.dto.TransactionType;
import com.rubrain.test.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RestController
@RequiredArgsConstructor
public class TransactionController {

    private static final String TRANSACTIONS_URI = "/transactions";

    private final TransactionService transactionService;

    @PostMapping(TRANSACTIONS_URI)
    public void createTransaction(@RequestParam TransactionType type,
                                  @RequestBody @Valid TransactionDto transactionDto) {
        transactionService.createAndExecuteTransaction(type, transactionDto);
    }
}
