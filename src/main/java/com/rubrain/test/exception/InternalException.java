package com.rubrain.test.exception;

import lombok.AllArgsConstructor;

import java.util.Optional;

import static java.util.Optional.ofNullable;

@AllArgsConstructor
public class InternalException extends RuntimeException {

    private Integer httpCode;

    public InternalException(String message, Integer httpCode) {
        super(message);
        this.httpCode = httpCode;
    }

    public InternalException(String message) {
        super(message);
    }

    public Optional<Integer> getHttpCode() {
        return ofNullable(httpCode);
    }
}
