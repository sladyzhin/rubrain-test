package com.rubrain.test.exception.handler;

import com.rubrain.test.exception.InternalException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class SpringWebExceptionHandler {

    @ExceptionHandler(value = {Throwable.class})
    public ResponseEntity<Object> exception(Throwable exception) {
        return handleException(new InternalException(exception.getMessage()));
    }

    @ExceptionHandler(value = {InternalException.class})
    public ResponseEntity<Object> internalException(InternalException exception) {
        return handleException(exception);
    }

    private ResponseEntity<Object> handleException(InternalException exception) {
        return new ResponseEntity<>(exception.getMessage(), getStatus(exception));
    }

    private HttpStatus getStatus(InternalException exception) {
        return exception.getHttpCode().map(HttpStatus::valueOf)
                .orElse(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}