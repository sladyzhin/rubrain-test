package com.rubrain.test.repository;

import com.rubrain.test.repository.entity.AccountEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface AccountRepository extends MongoRepository<AccountEntity, String> {

    Optional<AccountEntity> findByNumber(String number);

}
